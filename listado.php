<?php 

include 'conexion/conexion.php';

$sql_libros = "SELECT * FROM libros";
$consulta_libros = $conexion->query($sql_libros);

?>


<!DOCTYPE html>
<html lang="en">
<head>
<?php require 'extensiones/head.php' ?>
    <title>Listado de Libros Solicitados</title>
  
</head>
<body>

<?php require 'extensiones/nav.php'?>



    <div class="table-responsive" style="padding: 10%">
        <table class="table table-striped table-bordered table-hover" >
            <thead class="thead-light">
                <tr class="danger">
                    <th>ID</th>
                    <th>Titulo</th>
                    <th>Autor</th>
                    <th>Año de publicacion</th>
                    <th>Idioma</th>
                    <th>Opciones</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if($consulta_libros->num_rows > 0){
                    while ($libros = $consulta_libros->fetch_assoc()){                 
                ?>
                <tr>
                    <td><?php echo $libros['id']?></td>
                    <td><?php echo $libros['titulo']?></td>
                    <td><?php echo $libros['autor']?></td>
                    <td><?php echo $libros['ano']?></td>
                    <td><?php echo $libros['idioma']?></td>
                    <td>
                        <!-- BOTON PARA EDITAR LIBRO -->
                        <div class="row">
                            <div class="col-md-6">
                                <a href="editar-libro.php?id=<?php echo $libros['id']?>" class="btn btn-block" style="background: orange; color: white">Editar</a>
                            </div>
                                                 
                            <div class="col-md-6">
                                <a href="eliminar-libro.php?id=<?php echo $libros['id']?>" class="btn btn-block" style="background: red; color: white">Eliminar</a>
                            </div>                            
                        </div>
                        
                    </td>
                </tr>
                        </tr>
                    <?php }} ?>
            </tbody>    
        </table>
    </div>
        <div class="row">
                <div class="col-md-12">

                    <?php 
                        if(!empty($_GET['error'])){
                            $respuesta = $_GET['error'];
                            $contenido = $_GET['contenido'];
                    ?>

                    <?php
                        if($respuesta=='modificado'){?>                            
                                <div class="alert alert-primary" role="alert">                                
                                    <a href="#" class="alert-link"><?php echo $contenido?></a>
                                </div>                            
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

            <div class="row">
                        <div class="col-md-12">

                            <?php 
                                if(!empty($_GET['error'])){
                                    $respuesta = $_GET['error'];
                                    $contenido = $_GET['contenido'];
                            ?>

                            <?php
                                if($respuesta=='eliminado'){?>
                                <div class="col-md-12">
                                    <div class="alert alert-danger" role="alert">
                                        El libro ha sido eliminado.
                                    </div>
                                </div> 
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div>


    </div>
    <?php require 'extensiones/scripts.php' ?>
</body>
</html>