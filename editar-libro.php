<?php 
    include 'conexion/conexion.php';

    $id = $_GET['id'];

    $consulta = "SELECT * FROM libros WHERE id = $id";
    $result = $conexion->query($consulta);

    $result = $result->fetch_assoc();

?>


<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <title>Modificar Libros</title>
</head>
<body>
<?php require 'extensiones/nav.php' ?>


    <div class="contenedor">
        <div class="titulo">
            <h2>Modificar Libro</h2>            
            <hr>
        </div>
    

        <div class="formulario">
            <form action="modificar-libro.php" method ="GET">            
                <div class="form-group row">
                    <label for="id" class="col-sm-2 col-form-label">Id</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="id" name="id" value="<?php echo $result['id']?>"readonly>
                    </div>
                </div>  

                <div class="form-group row">
                    <label for="titulo" class="col-sm-2 col-form-label">Título</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="titulo" name="titulo" value="<?php echo $result['titulo']?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="autor" class="col-sm-2 col-form-label">Autor</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="autor" name="autor" value="<?php echo $result['autor']?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="ano" class="col-sm-2 col-form-label">Año de publicación</label>
                    <div class="col-sm-10">
                        <input type="number" class="form-control" id="ano" name="ano"value="<?php echo $result['ano']?>" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="idioma" class="col-sm-2 col-form-label">Idioma</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="idioma" name="idioma"value="<?php echo $result['idioma']?>" required>
                    </div>
                </div>

  <hr>
      <button type="submit" class="button-ingresar">Modificar Libro<i class="fas fa-angle-double-right"></i></button>

      

</form>
            


           
        </div>
    </div>

    <?php require 'extensiones/scripts.php' ?>
</body>
</html> 


