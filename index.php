<?php   include 'conexion/conexion.php' ?>

<!DOCTYPE html>
<html lang="en">
<head>
    <?php require 'extensiones/head.php' ?>
    <title>Registro de Libros</title>
</head>
<body>
<?php require 'extensiones/nav.php' ?>

    <div class="contenedor">
        <div class="titulo">
            <h2>Registro de Libros</h2>
            <hr>
        </div>
  
        <div class="formulario">
            <form action="agregar-libro.php" method ="GET">
            
  <div class="form-group row">
    <label for="id" class="col-sm-2 col-form-label">Id</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="id" name="id" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="titulo" class="col-sm-2 col-form-label">Título</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="titulo" name="titulo" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="autor" class="col-sm-2 col-form-label">Autor</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="autor" name="autor" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="ano" class="col-sm-2 col-form-label">Año de publicación</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="ano" name="ano" required>
    </div>
  </div>

  <div class="form-group row">
    <label for="idioma" class="col-sm-2 col-form-label">Idioma</label>
    <div class="col-sm-10">
      <input type="text" class="form-control" id="idioma" name="idioma" required>
    </div>
  </div>


  <hr>
      <button type="submit" class="button-ingresar">Registrar Libro<i class="fas fa-angle-double-right"></i></button>
  
</form>

                    <div class="row">
                        <div class="col-md-12">

                            <?php 
                                if(!empty($_GET['error'])){
                                    $respuesta = $_GET['error'];
                                    $contenido = $_GET['contenido'];
                            ?>

                            <?php
                                if($respuesta=='vacio'){?>
                                <div class="col-md-12">
                                    <div class="alert alert-success" role="alert">
                                        <h4 class="alert-heading">Bien Hecho!</h4>
                                        <p>El libro se ha registrado con éxito.</p>
                                        <hr>
                                        <p class="mb-0">Puedes registrar uno nuevo.</p>
                                    </div>
                                </div> 
                                <?php } ?>
                            <?php } ?>
                        </div>
                    </div> 
          </div>
    </div>

    <?php require 'extensiones/scripts.php' ?>
    
</body>
</html> 



